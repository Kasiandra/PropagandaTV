#!/bin/sh
# get rid of the cursor so we don't see it when videos are running
setterm -cursor off -background white -foreground white

# set here the path to the directory containing your videos
VIDEOPATH="/home/pi/Videos/" 

# you can normally leave this alone
SERVICE="omxplayer"

# now for our infinite loop!
while true; do
        if ps ax | grep -v grep | grep $SERVICE > /dev/null
        then
        sleep 2;
else
        for entry in $VIDEOPATH/*
        do
                clear
                omxplayer -r -o hdmi --aspect-mode fill $entry 1>/dev/null 2>/dev/null
        done
fi
done
